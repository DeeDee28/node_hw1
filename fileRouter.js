const express = require('express');
const router = express.Router();
const fs = require('fs').promises;
const { findFile } = require('./findFileMiddleware');

(async () => {
  try {
    await fs.mkdir('createdFiles');
  } catch {
    console.log('This folder already exists');
  }
})();

router.post('/', async (req, res) => {
  const { filename, content } = req.body;
  const regExp = /^.*\.(txt|log|json|yaml|xml|js)$/;

  if(!filename) {
    return res.status(400).json({"message": "Please specify 'filename' parameter"});
  }

  if(!content) {
    return res.status(400).json({"message": "Please specify 'content' parameter"});
  }

  if(!regExp.test(filename)) {
    return res.status(400).json({"message": "Inappropriate file format"});
  }

  try {
    await fs.writeFile('createdFiles/' + filename, content, 'utf-8');

    res.json({"message": "File created successfully"});
  } catch {
    return res.status(500).json({"message": "Server error"});
  }
})

router.get('/', async (req, res) => {
  try {
    const createdFilesData = await fs.readdir('createdFiles');

    res.json({'message': 'Success', 'files': createdFilesData})
  } catch {
      return res.status(500).json({"message": "Server error"});
  }
})

router.get('/:filename', findFile, async (req, res) => {
  const file = res.locals.file;

  try{
    const content = await fs.readFile('createdFiles/' + file, 'utf-8');
    const { birthtime } = await fs.stat('createdFiles/' + file);
    const extention = file.match(/\..+/)[0].slice(1);

    res.json({
      'message': 'Success',
      'filename': file,
      'content': content,
      'extension': extention,
      'uploadedDate': birthtime
    })
  } catch {
    return res.status(500).json({"message": "Server error"});
  }
})

router.put('/:filename', findFile, async (req, res) => {
  try {
    const { content } = req.body;

    if(!content) {
      return res.status(400).json({"message": "Please specify 'content' parameter"});
    }

    await fs.writeFile('createdFiles/' + res.locals.file, content, 'utf-8');

    res.json({"message": "File has been updated"});
  } catch {
    return res.status(500).json({"message": "Server error"});
  }
})

router.delete('/:filename', findFile, async (req, res) => {
  try {
    await fs.unlink('createdFiles/' + res.locals.file);

    res.json({"message": "File has been deleted"});
  } catch {
    return res.status(500).json({"message": "Server error"});
  }
})

module.exports = router;
