const fs = require('fs').promises;

module.exports.findFile = async (req, res, next) => {
  const createdFilesData = await fs.readdir('createdFiles');
  const { filename } = req.params;
  const file = createdFilesData.find(el => el === filename);
  
  if(!file) {
    return res.status(400).json({"message": `No file with '${filename}' filename found`});
  }

  res.locals.file = file;

  next()
}