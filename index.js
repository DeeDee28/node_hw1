const express = require('express');
const app = express();
const fileRouter = require('./fileRouter');

app.use(express.json());
app.use('/api/files', fileRouter);

app.listen(8080, async() => {
  console.log('Server has started');
})